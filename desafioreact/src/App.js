import logo from './logo.svg';
import './App.css';
import { FcSearch } from "react-icons/fc"
import { createElement, useState } from 'react';
import logo1 from './images/bkg1.png'

function LabelValue(label,valor, bool=true){
  return <div id='labelModal'><p>{label}</p><p className={bool?'':'end'}>{valor}</p></div>
}

function App() {
  var search
  const [array,setArray] = useState([])
  const [object, setObject] = useState({})
  const [isModalVisible, setIsModalVisible] = useState(false)
  

  function salvar (){
    search = (document.querySelector('input').value)
  }

  function request(){
    if(search){
      fetch(`https://api.github.com/search/users?q=${search}`).then((value)=>{
      value.json().then((res)=>{
      setArray(res.items)      
      }) 
      })
    }else{
      setArray('')
    }

  }

  function mkModal(login){
    fetch(`https://api.github.com/users/${login}`).then((value)=>{
      value.json().then((res)=>{
        setObject(res)
      })
    })
  }


  return (
    <div className="App" id='app'>
      <div style={{ display: 'flex', alignItems: 'center', height: 36, padding: 10 }}>
        <img src={logo1} style={{ height: '100%', marginLeft: 40, marginRight: 40}} />
        <input 
          style={{ flex: 1, height: '100%'}}
          type='search'
          value={search}
        />
        <button style={{ height: '100%' }} id='button' onClick={()=>{
          salvar()
          request()
        }}>
          <FcSearch />
        </button>
      </div>
      {/* 
         Logo + Search + Button
      
        Inicialmente - Logo 
        Apos clicar no botao - A pesquisa - os cards
      */}
     
      {/* <img id='logo' src={logo1} height={30} width={60}></img>
      <input 
        type='search'
        id='search'
        value={search}
      >
      </input>
      */}     
      {array.length > 0 ? 
      <section id='section'>
        <div id='list'>
          {array.map((element)=> 
            <div key={element.id} id='item'>
              <img id='img' src = {element.avatar_url} />
              <p id='login'>{element.login}</p>
              <a id='url' href={element.html_url}>{element.html_url}</a>
              <p id='score'>{element.score.toFixed(2)}</p>
              <button id='vermais' onClick={()=>{
                mkModal(element.login)
                setIsModalVisible(true)
                console.log(object)
                }
              }>
                VER MAIS</button>
            </div>
          )}
        </div>
      </section> : (
      <div id="logo-center" style={{ display: 'flex', alignItems: 'center', justifyContent: 'center',  width: '100%' }}>
        <img src={logo1} />
      </div>
      )}

      {isModalVisible? 
      <div id='modal'>
        <div id='container'>
        <img id='imgModal' src={object.avatar_url}/>
        <div id='content'>
        <div id='modalNome'>{object.name}</div>
        <div id='loginFollowers'>
        {LabelValue('Username:', object.login, true)}
        {LabelValue('Seguindo:', object.following, false)}
        </div>
        <div id='loginFollowers'>   
        {LabelValue('cadastrado(a):', object?.created_at?.replaceAll('-','/')?.slice(0,10), true)}
        {LabelValue('seguidores:', object.followers, false)}
        </div>
        {LabelValue('Url:', object.html_url)}
        <div id='divfechar'><button id='fechar' onClick={()=>setIsModalVisible(false)}>FECHAR</button></div>
        </div>
      </div>
      </div>
      :null}


    </div>
  );
}

export default App;
